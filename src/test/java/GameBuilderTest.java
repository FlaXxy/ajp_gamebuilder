import org.flaxxy.ajp.gamebuilder.Game;
import org.junit.Test;

import static org.flaxxy.ajp.gamebuilder.Game.*;

public class GameBuilderTest {
    @Test
    public void AutoCompletionTest() {
        Game g = Game.create().player(name("Donald"), age(50)).player(name("Hillary"), level(99)).asMaster().SetInSpace().player(name("Juan"))
                .adversary(Game.create().player(name("Kim Jong Un")).asMaster().SetUnderWater().get()).get();
    }
}
