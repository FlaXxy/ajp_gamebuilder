package org.flaxxy.ajp.gamebuilder;

public interface PlayersInfo extends GameInfo {
    GameInfo SetInSpace();

    GameInfo SetOnEarth();

    GameInfo SetUnderWater();

    GameInfo adversary(Game g);
}
