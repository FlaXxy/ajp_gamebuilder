package org.flaxxy.ajp.gamebuilder;

public interface PlayerAdder {
    PlayerInfo player(PlayerDetail... details);
}
