package org.flaxxy.ajp.gamebuilder;

public interface GameInfo extends PlayerAdder {
    Game get();
}
