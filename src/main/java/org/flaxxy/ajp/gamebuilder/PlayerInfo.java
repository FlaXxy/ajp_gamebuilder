package org.flaxxy.ajp.gamebuilder;

public interface PlayerInfo extends PlayersInfo {
    PlayersInfo asMaster();
}
