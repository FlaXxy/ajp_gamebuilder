package org.flaxxy.ajp.gamebuilder;

public class Game {
    Game() {

    }

    public static PlayerAdder create() {
        return new GameBuilder();
    }

    public static Name name(String s) {
        return new Name(s);
    }

    public static Level level(int l) {
        return new Level(l);
    }

    public static Age age(int a) {
        return new Age(a);
    }
}
