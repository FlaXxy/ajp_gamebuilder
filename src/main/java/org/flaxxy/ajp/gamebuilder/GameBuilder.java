package org.flaxxy.ajp.gamebuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class GameBuilder implements PlayerInfo, GameInfo {
    private List<Player> players = new ArrayList<Player>();
    private Setting setting = Setting.Undefined;
    private Game adversaryGame;

    public void setSetting(Setting value) {
        if (setting != Setting.Undefined)
            throw new IllegalArgumentException("Tried to Change final Setting " + this.setting + " to Setting " + value);
        setting = value;
    }

    public enum Setting {
        Undefined,
        Earth,
        Space,
        Underwater
    }

    public PlayersInfo asMaster() {
        players.get(players.size() - 1).IsMaster = true;
        return this;
    }

    public GameInfo SetInSpace() {
        setSetting(Setting.Space);
        return this;
    }

    public GameInfo SetOnEarth() {
        setSetting(Setting.Earth);
        return this;
    }

    public GameInfo SetUnderWater() {
        setSetting(Setting.Underwater);
        return this;
    }

    public GameInfo adversary(Game g) {
        if (adversaryGame != null)
            throw new IllegalArgumentException("Tried to set Adversary Game twice");
        adversaryGame = g;
        return this;
    }

    public Game get() {
        return new Game();
    }

    public PlayerInfo player(PlayerDetail... details) {
        players.add(new Player(details));
        return this;
    }
}
